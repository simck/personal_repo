// Copyright (c) 2022, libracore and contributors
// For license information, please see license.txt

frappe.ui.form.on('WooCommerce Sync', {
	onload: function(frm) {
		frm.disable_save();
	},
	refresh: function(frm) {
		frm.disable_save();
		frappe.call({
            method: "woocommerceconnector.api.get_log_status",
            callback: function(r) {
                if(r.message){
                    frm.dashboard.set_headline_alert(r.message.text, r.message.alert_class)
                }
            }
        })
	},
	sync_woocommerce: function(frm) {
		frappe.call({
            method:"woocommerceconnector.woocommerceconnector.doctype.woocommerce_sync.woocommerce_sync.get_woocommerce_status",
            callback: function(r) {
                if(r.message == 1){
                    frappe.call({
			            method:"woocommerceconnector.api.sync_woocommerce",
			        })
                }
            }
        })
		
	},
	woocommerce_log: function(frm) {
		frappe.set_route("List", "woocommerce Log");
	},
});
