# Copyright (c) 2022, libracore and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document
from woocommerceconnector.exceptions import woocommerceError
import frappe
from frappe import _

class WooCommerceSync(Document):
	pass

@frappe.whitelist()
def get_woocommerce_status():
	d = frappe.get_doc("WooCommerce Config")
	
	if d.enable_woocommerce:
		frappe.msgprint(_("WooCommerce sync commencing.."))
		return 1
	else:
		frappe.throw(_("WoocCommerce not enabled, please check with CloudE8."), woocommerceError)
		return 0

