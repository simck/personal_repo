// Copyright (c) 2022, libracore and contributors
// For license information, please see license.txt

frappe.ui.form.on('WooCommerce Shipping Zones', {
	// refresh: function(frm) {

	// }
	zone_regions: function(frm) {
		frappe.call({
			"method": "get_country_code",
			"doc": cur_frm.doc,
			callback: function(r){
				if (r.message) {
					frm.doc.country_code = r.message.toUpperCase();
					refresh_field("country_code");
				}
			}
		})
	}
});

frappe.ui.form.on("WooCommerce Shipping Methods", {
	shipping_methods: function(frm,cdt,cdn) {
		var row = locals[cdt][cdn];

		frappe.model.set_value(cdt, cdn, "title", row.shipping_methods);
	}
});