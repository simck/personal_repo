# Copyright (c) 2022, libracore and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class WooCommerceShippingZones(Document):
	@frappe.whitelist()
	def get_country_code(self):
		code = frappe.get_doc("Country", self.zone_regions)

		return code.code