from __future__ import unicode_literals
import frappe
from frappe.utils import cstr
from frappe import _
import requests.exceptions
from .exceptions import woocommerceError
from .utils import make_woocommerce_log, disable_woocommerce_sync_for_item
from erpnext.stock.utils import get_bin
from frappe.utils import cstr, flt, cint, get_files_path
from .woocommerce_requests import post_request, put_request, get_request_request
import base64, requests, datetime, os
from frappe.utils import get_datetime
from frappe.model.rename_doc import update_document_title

def sync_shippings_zones(woocommerce_settings):
	# try:
	sync_erp_shipping_to_woo(woocommerce_settings)
	create_erp_shipping_zone()
	# except woocommerceError as e:
	# 	make_woocommerce_log(title="{0}".format(e), status="Error", method="sync_shippings_zones", message=frappe.get_traceback(), exception=True)

def sync_erp_shipping_to_woo(woocommerce_settings):
	shipping_zones = frappe.db.sql("""SELECT z.name, z.zone_name, z.zone_id, z.country_code
		FROM `tabWooCommerce Shipping Zones` z
		WHERE z.modified > '{last_sync_datetime}'""".format(last_sync_datetime=woocommerce_settings.last_sync_datetime), as_dict=1)

	if shipping_zones:
		for zone in shipping_zones:
			if not zone.zone_id:
				woo_shipping_zone_id = post_new_shipping_zone(zone)
			else:
				woo_shipping_zone_id = zone.zone_id
				put_shipping_zone(zone)

			shipping_methods = frappe.db.sql("""SELECT m.name, m.method_id, m.shipping_methods, m.title, m.enabled, m.tax_status, m.cost, m.flatrate_shipping_class_cost, m.no_shipping_class_cost, m.calculation_type, m.free_shipping_requires, m.minimum_order_amount, m.coupons_discounts
				FROM `tabWooCommerce Shipping Methods` m
				WHERE m.parent = '{zone_name}'
				AND m.modified > '{last_sync_datetime}'""".format(zone_name=zone.name, last_sync_datetime=woocommerce_settings.last_sync_datetime), as_dict=1)

			for method in shipping_methods:
				if not method.method_id:
					woo_shipping_method_id = post_new_shipping_method(method, woo_shipping_zone_id)
				else:
					woo_shipping_method_id = method.method_id
					put_shipping_method(method, woo_shipping_zone_id)

def create_erp_shipping_zone():
	for zone in get_woocommerce_shipping_zones():
		if zone.get("id") > 0:
			if not frappe.db.exists("WooCommerce Shipping Zones", {"zone_id": zone.get("id")}):
				for region in get_woocommerce_shipping_locations(zone.get("id")):
					country = frappe.get_doc("Country", {"code": region.get("code").lower()})

					methods_list = []

					for method in get_woocommerce_shipping_methods(zone.get("id")):
						enabled = 0

						if method.get("id") > 0:
							if method.get("enabled"):
								enabled = 1

							method_dict = {
								"method_id": method.get("id")
								, "enabled": enabled
								, "shipping_methods": method.get("method_title")
								, "title": method.get("title")
							}

							settings = method.get("settings")

							if "tax_status" in settings:
								cost_dict = settings.get("tax_status")

								if cost_dict.get("id") == "tax_status":
									if cost_dict.get("value"):
										if cost_dict.get("value") == "taxable":
											method_dict["tax_status"] = "Taxable"
										elif cost_dict.get("value") == "none":
											method_dict["tax_status"] = "None"

							if "cost" in settings:
								cost_dict = settings.get("cost")

								if cost_dict.get("id") == "cost":
									if cost_dict.get("value"):
										method_dict["cost"] = cost_dict.get("value")

							if "class_cost_158" in settings:
								cost_dict = settings.get("class_cost_158")

								if cost_dict.get("id") == "class_cost_158":
									if cost_dict.get("value"):
										method_dict["flatrate_shipping_class_cost"] = cost_dict.get("value")

							if "no_class_cost" in settings:
								cost_dict = settings.get("no_class_cost")

								if cost_dict.get("id") == "no_class_cost":
									if cost_dict.get("value"):
										method_dict["no_shipping_class_cost"] = cost_dict.get("value")

							if "type" in settings:
								cost_dict = settings.get("type")

								if cost_dict.get("id") == "type":
									if cost_dict.get("value"):
										if cost_dict.get("value") == "class":
											method_dict["calculation_type"] = "Per class: Charge shipping for each shipping class individually"
										elif cost_dict.get("value") == "order":
											method_dict["calculation_type"] = "Per order: Charge shipping for the most expensive shipping class"

							if "requires" in settings:
								cost_dict = settings.get("requires")

								if cost_dict.get("id") == "requires":
									if not cost_dict.get("value"):
										method_dict["free_shipping_requires"] = 'N/A'
									elif cost_dict.get("value") == "coupon":
										method_dict["free_shipping_requires"] = "A valid free shipping coupon"
									elif cost_dict.get("value") == "min_amount":
										method_dict["free_shipping_requires"] = "A minimum order amount"
									elif cost_dict.get("value") == "either":
										method_dict["free_shipping_requires"] = "A minimum order amount OR a coupon"
									elif cost_dict.get("value") == "both":
										method_dict["free_shipping_requires"] = "A minimum order amount AND a coupon"

							if "min_amount" in settings:
								cost_dict = settings.get("min_amount")

								if cost_dict.get("id") == "min_amount":
									if cost_dict.get("value"):
										method_dict["minimum_order_amount"] = cost_dict.get("value")

							if "ignore_discounts" in settings:
								cost_dict = settings.get("ignore_discounts")

								if cost_dict.get("id") == "ignore_discounts":
									if cost_dict.get("value"):
										if cost_dict.get("value") == "no":
											method_dict["coupons_discounts"] = 0
										elif cost_dict.get("value") == "yes":
											method_dict["coupons_discounts"] = 1

							methods_list.append(method_dict)

					shipping_zones = frappe.get_doc({
						"doctype": "WooCommerce Shipping Zones"
						, "zone_id": zone.get("id")
						, "zone_name": zone.get("name")
						, "zone_regions" : country.name
						, "shipping_methods": methods_list,
					})

					try:
						shipping_zones.save()
					except Exception as err:
						make_woocommerce_log(title="Create New Shipping Zones", status="Error", method="create_erp_shipping_zone", message=err, request_data=None, exception=True)

def post_new_shipping_zone(zone):
	woocommerce_shipping_zone = post_request("shipping/zones", {'name': zone.zone_name})
	put_request("shipping/zones/{0}/locations".format(woocommerce_shipping_zone.get("id")), [{'code': zone.country_code, 'type': 'country'}])

	frappe.db.set_value("WooCommerce Shipping Zones", zone.name, "zone_id", woocommerce_shipping_zone.get("id"))

	return woocommerce_shipping_zone.get("id")

def post_new_shipping_method(method, woo_shipping_zone_id):
	if method.enabled:
		enabled = True
	else:
		enabled = False

	if method.shipping_methods == "Flat rate":
		if method.tax_status == "Taxable":
			tax_status = "taxable"
		elif method.tax_status == "None":
			tax_status = "none"

		if method.calculation_type == "Per class: Charge shipping for each shipping class individually":
			calculation_type = "class"
		elif method.calculation_type == "Per order: Charge shipping for the most expensive shipping class":
			calculation_type = "order"

		settings = {'title': method.title, 'tax_status': tax_status, 'cost': method.cost, 'class_cost_158': method.flatrate_shipping_class_cost, 'no_class_cost': method.no_shipping_class_cost, 'type': calculation_type}
		method_details = {'method_id': 'flat_rate', 'enabled': enabled, 'settings': settings}

	if method.shipping_methods == "Free shipping":
		if method.free_shipping_requires == "N/A":
			free_shipping_requires = ""
		elif method.free_shipping_requires == "A valid free shipping coupon":
			free_shipping_requires = "coupon"
		elif method.free_shipping_requires == "A minimum order amount":
			free_shipping_requires = "min_amount"
		elif method.free_shipping_requires == "A minimum order amount OR a coupon":
			free_shipping_requires = "either"
		elif method.free_shipping_requires == "A minimum order amount AND a coupon":
			free_shipping_requires = "both"

		if method.coupons_discounts:
			coupons_discounts = "yes"
		else:
			coupons_discounts = "no"

		settings = {'title': method.title, 'requires': free_shipping_requires, 'min_amount': method.minimum_order_amount, 'ignore_discounts': coupons_discounts}
		method_details = {'method_id': 'free_shipping', 'enabled': enabled, 'settings': settings}

	if method.shipping_methods == "Local pickup":
		if method.tax_status == "Taxable":
			tax_status = "taxable"
		elif method.tax_status == "None":
			tax_status = "none"

		settings = {'title': method.title, 'tax_status': tax_status, 'cost': method.cost}
		method_details = {'method_id': 'local_pickup', 'enabled': enabled, 'settings': settings}

	woocommerce_shipping_method = post_request("shipping/zones/{0}/methods".format(woo_shipping_zone_id), method_details)

	frappe.db.set_value("WooCommerce Shipping Methods", method.name, "method_id", woocommerce_shipping_method.get("id"))

	return woocommerce_shipping_method.get("id")

	# method = post_request("shipping/zones/{0}/methods".format(woocommerce_shipping_zone.get("id")), {'settings': {'title': 'Delivery'}})

def put_shipping_zone(zone):
	put_request("shipping/zones/{0}".format(zone.zone_id), {'name': zone.zone_name})
	put_request("shipping/zones/{0}/locations".format(zone.zone_id), [{'code': zone.country_code, 'type': 'country'}])

def put_shipping_method(method, woo_shipping_zone_id):
	if method.enabled:
		enabled = True
	else:
		enabled = False

	if method.shipping_methods == "Flat rate":
		if method.tax_status == "Taxable":
			tax_status = "taxable"
		elif method.tax_status == "None":
			tax_status = "none"

		if method.calculation_type == "Per class: Charge shipping for each shipping class individually":
			calculation_type = "class"
		elif method.calculation_type == "Per order: Charge shipping for the most expensive shipping class":
			calculation_type = "order"

		settings = {'title': method.title, 'tax_status': tax_status, 'cost': method.cost, 'class_cost_158': method.flatrate_shipping_class_cost, 'no_class_cost': method.no_shipping_class_cost, 'type': calculation_type}
		method_details = {'method_id': 'flat_rate', 'enabled': enabled, 'settings': settings}

	if method.shipping_methods == "Free shipping":
		if method.free_shipping_requires == "N/A":
			free_shipping_requires = ""
		elif method.free_shipping_requires == "A valid free shipping coupon":
			free_shipping_requires = "coupon"
		elif method.free_shipping_requires == "A minimum order amount":
			free_shipping_requires = "min_amount"
		elif method.free_shipping_requires == "A minimum order amount OR a coupon":
			free_shipping_requires = "either"
		elif method.free_shipping_requires == "A minimum order amount AND a coupon":
			free_shipping_requires = "both"

		if method.coupons_discounts:
			coupons_discounts = "yes"
		else:
			coupons_discounts = "no"

		settings = {'title': method.title, 'requires': free_shipping_requires, 'min_amount': method.minimum_order_amount, 'ignore_discounts': coupons_discounts}
		method_details = {'method_id': 'free_shipping', 'enabled': enabled, 'settings': settings}

	if method.shipping_methods == "Local pickup":
		if method.tax_status == "Taxable":
			tax_status = "taxable"
		elif method.tax_status == "None":
			tax_status = "none"

		settings = {'title': method.title, 'tax_status': tax_status, 'cost': method.cost}
		method_details = {'method_id': 'local_pickup', 'enabled': enabled, 'settings': settings}

	put_request("shipping/zones/{0}/methods/{1}".format(woo_shipping_zone_id, method.method_id), method_details)

def get_woocommerce_shipping_zones():
	woocommerce_shipping_zones = []

	response = get_request_request('shipping/zones')
	woocommerce_shipping_zones.extend(response.json())

	return woocommerce_shipping_zones

def get_woocommerce_shipping_locations(zone_id):
	woocommerce_shipping_locations = []

	response = get_request_request('shipping/zones/{0}/locations'.format(zone_id))
	woocommerce_shipping_locations.extend(response.json())

	return woocommerce_shipping_locations

def get_woocommerce_shipping_methods(zone_id):
	woocommerce_shipping_methods = []

	response = get_request_request('shipping/zones/{0}/methods'.format(zone_id))
	woocommerce_shipping_methods.extend(response.json())

	return woocommerce_shipping_methods