from os.path import basename, join, isfile, isdir, realpath, expanduser, dirname, exists, splitext
from glob import glob
import threading
from time import sleep, time
from os import walk, makedirs, system
from multiprocessing import cpu_count
import queue

try:
	import boto3
	import boto3.session
except (ModuleNotFoundError, ImportError):
	system('pip3 install boto3')
	import boto3
	import boto3.session

class FileUploadToS3(object):
	def __init__(self, region='ap-southeast-1'):
		self.num_workers = min(cpu_count() * 4, 16)
		self.max_queue_size = self.num_workers * 4
		self.queue = queue.Queue(maxsize=self.max_queue_size)
		self._stop_event = None
		self._threads = []
		self.wait_time = .1
		self.my_name = 'Thread'
		self.region = region
		self.load_config()
		self._credentials = self.load_credentials()
		print (self.config)
		self.s3 = boto3.client(
			's3',
			region_name=self.region,
			aws_access_key_id=self._credentials[self._credentials_keys[0]],
			aws_secret_access_key=self._credentials[self._credentials_keys[1]]		
			)
		
	def load_credentials(self):
		self._credentials_keys = ['aws_access_key_id', 'aws_secret_access_key']
		credentials_path = join(dirname(realpath(__file__)), "credentials")
		credentials = {}
		if not exists(credentials_path) or not isfile(credentials_path):
			raise Exception ('Cannot find the credential file {}'.format(credentials_path))
		with open(credentials_path, 'r') as f:
			for line in f.readlines():
				if '=' in line and any([_ in line for _ in self._credentials_keys]):
					segs = line.strip().split('=')
					credentials[segs[0].strip()] = '='.join(segs[1:]).strip()
					if len(credentials[segs[0].strip()]) == 0:
						raise Exception ('Empty credential: {}'.format(segs[0].strip()))
		if len(self._credentials_keys) != len(credentials):
			raise Exception ('Cannot find {} from {}'.format(
				[_ for _ in self._credentials_keys if _ not in credentials], credentials_path))
		return credentials	

	def load_config(self):
		config_keys = ['bucket_name', 's3_folder_name', 'local_folder_to_upload']
		config_path = join(dirname(realpath(__file__)), "config")
		config = {}
		if not exists(config_path) or not isfile(config_path):
			raise Exception ('Cannot find the config file {}'.format(config_path))

		with open(config_path, 'r') as f:
			for line in f.readlines():
				if '=' in line and any([_ in line for _ in config_keys]):
					segs = line.strip().split('=')
					key = segs[0].strip()
					value = '='.join(segs[1:]).strip()
					if key == config_keys[0] and not value:
						raise Exception ("Please set Bucket Name in the config")
					if key == config_keys[2]:
						if not value:
							raise Exception ("Please specify the local folder to upload")
						value = join(dirname(realpath(__file__)), value)
					config[key] = value
					
		self.config = config		
	
	def check_files(self, bucket_name, num_print=float('inf'), verb=True):
		file_list = []
		if bucket_name in self.get_bucket_names():
			cnt = 0
			s3 = boto3.resource(
				's3',
				region_name=self.region,
				aws_access_key_id=self._credentials[self._credentials_keys[0]],
				aws_secret_access_key=self._credentials[self._credentials_keys[1]]			
				)
			bucket = s3.Bucket(bucket_name)
			for obj in bucket.objects.all():
				file_list.append(obj.key)
				cnt += 1
				if verb:
					print(file_list[-1])
				if cnt >= num_print:
					break
			return file_list
		else:
			raise Exception ('The Bucket {} does not exist!'.format(bucket_name))

	def get_bucket_names(self):
		return [bucket['Name'] for bucket in self.s3.list_buckets()['Buckets']]	

	def create_bucket(self, bucket_name):
		if bucket_name in self.get_bucket_names():
			raise Exception ('The Bucket {} already exist!'.format(bucket_name))
		else:
			try:
				self.s3.create_bucket(Bucket=bucket_name, CreateBucketConfiguration={'LocationConstraint': self.region})
			except Exception as e:
				raise Exception ('Not able to create Bucket. Error : {}'.format(e))	

	def download(self, bucket_name, prefix='', save_dir='./', verb=True):
		if not exists(save_dir) or not isdir(save_dir):
			makedirs(save_dir)

		if not prefix:
			file_list = self.check_files(bucket_name=bucket_name, verb=False)
			print('Find {} files in Bucket {}'.format(len(file_list), bucket_name))
			if input('Download all files?[y/n]'.format(bucket_name)) != 'y':
				exit(0)

		session = boto3.session.Session(
			region_name=self.region,
			aws_access_key_id=self._credentials[self._credentials_keys[0]],
			aws_secret_access_key=self._credentials[self._credentials_keys[1]]
		)
		s3 = session.resource('s3')
		bucket = s3.Bucket(bucket_name)
		for obj in bucket.objects.filter(Prefix=prefix):
			save_path = join(save_dir, obj.key)
			if not exists(dirname(save_path)) or not isdir(dirname(save_path)):
				makedirs(dirname(save_path))
			if verb:
				print('\rDownloading Bucket {}/{} -> {}'.format(bucket_name, obj.key, save_path))
			try:
				bucket.download_file(obj.key, save_path)
			except Exception as e:
				print('[!] Failed download {}/{}: {}'.format(bucket_name, obj.key, e))

	def load_files(self, data_dir, regex=('*.*',)):
		assert isinstance(regex, (list, tuple)) and len(regex) > 0
		files_path = []
		for ex in regex:
			for root, subdirs, files in walk(data_dir):
				files_path.extend([_ for _ in glob(join(root, ex)) if isfile(_)])
		return files_path			
	
	def __call__(self, is_public=False, regex=('*.*',), verb=False):
		"""
			data_to_upload: str, dir to the data that may include subdirs, or path to a file
			bucket_name: str, name of the bucket, must be lower-case
			bucket_folder: str, folder path under the bucket to store data
			is_public: bool, whether make the data to be public (accessible publicly)
			regex: tuple of str, file patterns to upload, e.g., ('*.jpg', '.*png') to upload jpg and png images
			verb: bool, whether print uploading info
		"""
		bucket_name = self.config.get('bucket_name')
		bucket_folder = self.config.get('s3_folder_name')
		data_to_upload = self.config.get('local_folder_to_upload')
		assert isinstance(data_to_upload, str) and exists(data_to_upload)
		if not bucket_name:
			raise Exception ("Bucket Name is required")
		if isfile(data_to_upload):
			key = basename(data_to_upload) if bucket_folder is None else join(bucket_folder, basename(data_to_upload))
			self.s3.upload_file(data_to_upload, bucket_name, key, ExtraArgs={'ACL': 'public-read' if is_public else ''})
		else:  # upload files in a folder
			files = self.load_files(data_dir=data_to_upload, regex=regex)
			self._stop_event = threading.Event()
			self.start(bucket_name=bucket_name, bucket_folder=bucket_folder, is_public=is_public, verb=verb)
			num_total_files = len(files)
			f = open('file_list.txt', 'w')
			for file in files:
				key = data_to_upload.join(file.split(data_to_upload)[1:])
				if key[0] in ['/', '\\']:
					key = key[1:]
				self.queue.put([file, key])
				f.writelines(key + '\n')
			f.close()

			while not self.queue.empty():
				sleep(1)

			self.stop()
		print('Done: {0} file(s) uploaded to Bucket {1}'.format(num_total_files, bucket_name))

	def is_running(self):
		return self._stop_event is not None and not self._stop_event.is_set()

	def stop(self, timeout=None):
		if self.is_running():
			self._stop_event.set()

		for thread in self._threads:
			if thread.is_alive():
				thread.join(timeout)
		self._threads = []
		self._stop_event = None
		self.queue = None

	def start(self, **kwargs):
		for idx in range(self.num_workers):
			try:
				thread = threading.Thread(target=self._upload, name=str(idx), kwargs=kwargs, daemon=False)
				self._threads.append(thread)
				thread.start()
			except:
				import traceback
				traceback.print_exc()

	def _upload(self, bucket_name, bucket_folder=None, is_public=False, verb=False):
		session = boto3.session.Session(
			region_name=self.region,
			aws_access_key_id=self._credentials[self._credentials_keys[0]],
			aws_secret_access_key=self._credentials[self._credentials_keys[1]]
		)
		
		s3 = session.resource('s3')
		while self.is_running():
			try:
				if not self.queue.empty():
					data_path, key = self.queue.get()
					if not bucket_folder and bucket_folder != '':
						key_list = [bucket_folder, key]
						key = "/".join(key_list)
					if verb:
						print('Uploading {} -> Bucket {}/{}'.format(data_path, bucket_name, key))
					if is_public:
						s3.meta.client.upload_file(Filename=data_path, Bucket=bucket_name, Key=key,
												   ExtraArgs={'ACL': 'public-read'})
					else:
						s3.meta.client.upload_file(Filename=data_path, Bucket=bucket_name, Key=key)
				else:
					sleep(self.wait_time)
			except:
				import traceback
				traceback.print_exc()

uploader = FileUploadToS3()	
uploader()
